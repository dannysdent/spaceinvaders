Training Space Invaders using NEAT
===========


About
-----
Space Invader Project.

The goal of the game was to make the Cyber Space Invader game smart by implementing machine learning. Effective position of the rocket value position are learned through a training procedure. This yields a situation whereby a trial and error process may be necessary in order to determine an appropriate topology for the rocket. The game will be played for a couple of times as it becomes smart gradually.
NEAT(NeuroEvolution of Augmenting Topologies) python was used to train the game.

You will need the following installed.
----

   Python: 3.6.9

   NEAT-Python: 0.92
   
   Pygame: 1.9.3
   
   Numpy: 1.13.3
   
   Scipy: 0.16.1
   
   Sklearn: 0.19.0
   
However, you can install  [Anaconda](https://anaconda.org/) for easier set up since some of the packages are already pre-installed.
 - If you have the correct version of all that installed, you can run the program in the command prompt / terminal.
 ``` bash
python space_driver.py
 ```

Daniel Sirali
-----
SCT211-8835/2015
-----

